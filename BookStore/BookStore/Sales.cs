﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BookStore
{
    public partial class Sales : Form
    {
        public Sales()
        {
            InitializeComponent();
        }

        private void Sales_Load(object sender, EventArgs e)
        {
            ConnectionDB.connectsql();
            setClear();
        }

        private void pictHome_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        // button color
        private static Color mp = Color.SteelBlue;
        private static Color mpl = Color.LightSkyBlue;
        private void mpHome(object sender, EventArgs e)
        {
            pictHome.BackColor = mp;
        }

        private void mplHome(object sender, EventArgs e)
        {
            pictHome.BackColor = mpl;
        }

        private void docID()
        {
            int id = 0;// error load
            DataTable loadDocID = ConnectionDB.executsql("SELECT COUNT(SaleID) as countID FROM Sale");
            id = Convert.ToInt32(loadDocID.Rows[0]["countID"].ToString())+1;
                  int countNum = Convert.ToInt32(id.ToString().Length);
                  switch (countNum)
                  {
                      case 1:
                          {
                              tbDocID.Text = "000000000000" + id;
                              break;
                          }
                      case 2:
                          {
                              tbDocID.Text = "00000000000" + id;
                              break;
                          }
                      case 3:
                          {
                              tbDocID.Text = "0000000000" + id;
                              break;
                          }
                      case 4:
                          {
                              tbDocID.Text = "000000000" + id;
                              break;
                          }
                      case 5:
                          break;
                      case 6:
                          {
                              tbDocID.Text = "00000000" + id;
                              break;
                          }
                      case 7:
                          {
                              tbDocID.Text = "0000000" + id;
                              break;
                          }
                      case 8:
                          {
                              tbDocID.Text = "000000" + id;
                              break;
                          }
                      case 9:
                          {
                              tbDocID.Text = "0000" + id;
                              break;
                          }
                      case 10:
                          {
                              tbDocID.Text = "000" + id;
                              break;
                          }
                      case 11:
                          {
                              tbDocID.Text = "00" + id;
                              break;
                          }
                      case 12:
                          {
                              tbDocID.Text = "0" + id;
                              break;
                          }
                      default:
                          {
                              // id 13 char
                              tbDocID.Text = id.ToString();
                              break;
                          }

                  }
        }

        private void txchinputProductID(object sender, EventArgs e)
        {
            
            DataTable productIDInput = ConnectionDB.executsql("SELECT * FROM Store WHERE ISBN = '"+tbProductID.Text+"'");
            if (productIDInput.Rows.Count>0)
            {
                // highlight
                nupQty.Focus();
                nupQty.Value = 1;
                nupQty.Select(0, nupQty.Text.Length);
            }
            else
            {
                if (tbProductID.Text.Length == 13)
                {
                    MessageBox.Show("กรุณากรอกรหัสหนังสือให้ถูกต้อง", "แจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    tbProductID.Clear();
                    tbProductID.Focus();
                }
                nupQty.Value = 0;
            }
        }

        private void addProductToList()
        {
            DataTable product = ConnectionDB.executsql("SELECT Name, Cost FROM Store WHERE ISBN = '" + tbProductID.Text + "'");
            if (product.Rows.Count == 0)
            {
                MessageBox.Show("กรุณากรอกรหัสหนังสือให้ถูกต้อง", "แจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tbProductID.Clear();
                tbProductID.Focus();
                return;
            }

            // show in DGV
            string bookname = product.Rows[0][0].ToString();
            double cost = Convert.ToDouble(product.Rows[0][1]);
            double total = Convert.ToDouble(nupQty.Value) * cost;
            dgvSales.Rows.Add(tbProductID.Text, bookname, nupQty.Value, cost, total);

            // last row focus when insert
            int dgvcount = dgvSales.Rows.Count;
            dgvSales.CurrentCell = dgvSales.Rows[dgvcount - 1].Cells[0];

            showTotal();
            checkDiscountForCustomer();

            // go to next product
            tbProductID.Clear();
            tbProductID.Focus();
        }

        private void showTotal()
        {
            // show total (sum)
            double sum = 0;
            for (int i = 0; i < dgvSales.Rows.Count; i++)
            {
                sum += Convert.ToInt32(dgvSales.Rows[i].Cells[4].Value);
            }
            tbTotal.Text = sum.ToString("0.00");

        }

        private void kdownEnterAddProduct(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                addProductToList();
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (dgvSales.Rows.Count == 0)
            {
                MessageBox.Show("กรุณาเลือกรายการที่จะยกเลิกการซื้อ","แจ้งเตือน",MessageBoxButtons.OK,MessageBoxIcon.Warning);
                return;
            }
            dgvSales.Rows.RemoveAt(dgvSales.CurrentCell.RowIndex);
            showTotal();
        }

        private void pressNumber(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < 48 || e.KeyChar > 57) && (e.KeyChar != 8) && (e.KeyChar != 13) && (e.KeyChar != 46))
            {
                e.Handled = true;
                MessageBox.Show("กรุณากรอกข้อมูลเฉพาะตัวเลข", "แจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void txchinputMoney(object sender, EventArgs e)
        {
            showReturnMenory();
        }

        private void showReturnMenory()
        {
            if (tbGetMoney.Text == "")
            {
                // break when delete getMoney
                tbReturnMoney.Text = (0).ToString("0.00");
                return;
            }
            double returnMoney = Convert.ToDouble(tbGetMoney.Text) - Convert.ToDouble(tbTotal.Text);
            tbReturnMoney.Text = returnMoney.ToString("0.00");
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            addProductToList();
        }

        private void txchinputCusID(object sender, EventArgs e)
        {
            DataTable cusIDInput = ConnectionDB.executsql("SELECT * FROM Customer WHERE CusID = '" + tbCusID.Text + "'");
            if (cusIDInput.Rows.Count > 0)
            {
                chDiscount.Checked = true;
            }
            else
            {
                if(tbCusID.Text.Length == 13)
                {
                    MessageBox.Show("กรุณากรอกรหัสลูกค้าให้ถูกต้อง", "แจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    tbCusID.Clear();
                    tbCusID.Focus();
                }
                chDiscount.Checked = false;
            }
        }

        private void checkDiscountForCustomer()
        {
            if(chDiscount.Checked == true)
            {
                double cost = (Convert.ToDouble(tbTotal.Text) * 95) / 100;
                tbTotal.Text = cost.ToString("0.00");  
            }
            if (chDiscount.Checked == false)
            {
                showTotal();
            }

        }

        private void checkDiscountTF(object sender, EventArgs e)
        {
            checkDiscountForCustomer();
        }

        private void txchTotalM(object sender, EventArgs e)
        {
            showReturnMenory();
        }

        private void btnSales_Click(object sender, EventArgs e)
        {
            checkCusIDTFtoSave();
        }

        private void checkCusIDTFtoSave()
        {
            DataTable cusIDInput = ConnectionDB.executsql("SELECT * FROM Customer WHERE CusID = '" + tbCusID.Text + "'");
            if (cusIDInput.Rows.Count > 0||tbCusID.Text=="")
            {
                saveDataSales();
            }
            else
            {
                tbCusID.Clear();
            }
        }

        private void kdownEnterSave(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                checkCusIDTFtoSave();
            }
        }

        private void saveDataSales()
        {
            
            if (dgvSales.Rows.Count == 0)
            {
                MessageBox.Show("กรุณาทำรายการซื้อก่อนดำเนินการบันทึกข้อมูล", "แจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tbProductID.Focus();
                return;
            }

            if (MessageBox.Show("คุณต้องการบันทึกข้อมูลการขายหนังสือหรือไม่", "แจ้งเตือน", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                ConnectionDB.executsql("INSERT INTO Sale (SaleID,Date,Total,GetMoney,ReturnMoney,CusID,UserID) VALUES('" + tbDocID.Text + "',GETDATE(),'" + tbTotal.Text + "','" + tbGetMoney.Text + "','" + tbReturnMoney.Text + "','" + tbCusID.Text + "', '"+textUserName.Text+"')"); //save main

                for (int i = 0; i < dgvSales.Rows.Count; i++)
                {
                    ConnectionDB.executsql("INSERT INTO SaleDetail (SaleID ,ISBN ,BookName ,Quantity ,Cost ,TotalCost) VALUES ('" + tbDocID.Text + "','" + dgvSales.Rows[i].Cells[0].Value.ToString() + "','" + dgvSales.Rows[i].Cells[1].Value.ToString() + "','" + dgvSales.Rows[i].Cells[2].Value.ToString() + "','" + dgvSales.Rows[i].Cells[3].Value.ToString() + "','" + dgvSales.Rows[i].Cells[4].Value.ToString() + "')"); //save saledetail
                                                                                                                                                                                                                                                                                                                                                                                                                                   //  ConnectionDB.executsql("UPDATE TB_Inventory SET Product_QTY = Product_QTY-'" + dgvSales.Rows[i].Cells[3].Value.ToString() + "'WHERE Product_ID = '" + dgv.Rows[i].Cells[0].Value.ToString() + "'"); //cutstock
                }

                PrintBill bill = new PrintBill();
                bill.textDocID = tbDocID.Text;
                // controlBill
                if (tbCusID.Text != "")
                {
                    bill.member = true;
                }
                else
                {
                    bill.member = false;
                }
                bill.ShowDialog();

                MessageBox.Show("บันทึกข้อมูลเรียบร้อย", "แจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Information);
                setClear();
            }
            

        }

        private void setClear()
        {
            dgvSales.Rows.Clear();     
            tbCusID.Clear();
            tbProductID.Clear();
            tbGetMoney.Clear();
            double b = 0;
            tbTotal.Text = b.ToString("0.00");
            tbReturnMoney.Text = b.ToString("0.00");
            this.ActiveControl = tbProductID;
            docID();
        }

        public string getdocumentID()
        {
            return tbDocID.Text;
        }
    }
}
