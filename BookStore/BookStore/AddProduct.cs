﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BookStore
{
    public partial class AddProduct : Form
    {
        public AddProduct()
        {
            InitializeComponent();
        }
        private void AddUser_Load(object sender, EventArgs e)
        {
            ConnectionDB.connectsql();
        }

        private void pictOK_Click(object sender, EventArgs e)
        {
            checkDatatoAdd();
        }

        private void pictCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void checkDatatoAdd()
        {
            if (tbISBN.Text == "" || tbBookName.Text == "" || tbAuthor.Text == "" || tbPublisher.Text == "" || tbPrintYear.Text == "" || tbType.Text == ""||tbQuantity.Text ==""||tbCost.Text =="")
            {
                MessageBox.Show("กรุณากรอกข้อมูลให้ครบถ้วน", "แจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (tbISBN.Text.Length < 13)
            {
                MessageBox.Show("กรุณากรอกรหัสหนังสือให้ครบ 13 หลัก", "แจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tbISBN.Focus();
                return;
            }

            //Check userID Used to in db
            DataTable checkdataISBN = ConnectionDB.executsql("SELECT * FROM Store WHERE ISBN='" + tbISBN.Text + "'");
            if (checkdataISBN.Rows.Count > 0)
            {
                MessageBox.Show("รหัสหนังสือนี้ถูกใช้ไปแล้ว กรุณากรอกรหัสหนังสือใหม่ให้ถูกต้อง", "แจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tbISBN.Clear();
                tbISBN.Focus();
                return;
            }

            ConnectionDB.executsql("INSERT INTO Store (ISBN,Name,Author,Publisher,PrintYear,Type,Quantity,Cost) VALUES('" + tbISBN.Text + "','" + tbBookName.Text + "','" + tbAuthor.Text + "','" + tbPublisher.Text + "','" + tbPrintYear.Text + "','" + tbType.Text + "','"+tbQuantity.Text+"','"+tbCost.Text+"')");
            MessageBox.Show("บันทึกข้อมูลเรียบร้อยแล้ว", "แจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.Close();
        }

        private void pressNumber(object sender, KeyPressEventArgs e)
        {
            /* other
            if (e.Handled = !char.IsDigit(e.KeyChar))
            {
                MessageBox.Show("กรุณากรอกข้อมูลเฉพาะตัวเลข", "แจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }*/

            if ((e.KeyChar < 48 || e.KeyChar > 57) && (e.KeyChar != 8) && (e.KeyChar != 13) && (e.KeyChar != 46))
            {
                e.Handled = true;
                MessageBox.Show("กรุณากรอกข้อมูลเฉพาะตัวเลข", "แจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void pressEnter(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                checkDatatoAdd();
            }
        }

        //button zoom
        private void mpOK(object sender, EventArgs e)
        {
            pictOK.Size = new System.Drawing.Size(60, 60);
        }

        private void mplOK(object sender, EventArgs e)
        {
            pictOK.Size = new System.Drawing.Size(50, 50);
        }

        private void mpCancel(object sender, EventArgs e)
        {
            pictCancel.Size = new System.Drawing.Size(60, 60);
        }

        private void mplCancel(object sender, EventArgs e)
        {
            pictCancel.Size = new System.Drawing.Size(50, 50);
        }

       
    }
}
