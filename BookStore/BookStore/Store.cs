﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BookStore
{
    public partial class Store : Form
    {
        public Store()
        {
            InitializeComponent();
        }

        private void Store_Load(object sender, EventArgs e)
        {
            ConnectionDB.connectsql();
            LoadData();
        }
        
        private void LoadData()
        {
            // DataGridviewLoadData
            DataTable loaddata = ConnectionDB.executsql("SELECT * FROM Store");
            dgvStore.DataSource = loaddata;
            dgvStore.Columns[0].HeaderText = "ISBN";
            dgvStore.Columns[1].HeaderText = "ชื่อหนังสือ";
            dgvStore.Columns[2].HeaderText = "ชื่อผู้แต่ง";
            dgvStore.Columns[3].HeaderText = "สำนักพิมพ์";
            dgvStore.Columns[4].HeaderText = "ปีที่พิมพ์";
            dgvStore.Columns[5].HeaderText = "หมวดหมู่";
            dgvStore.Columns[6].HeaderText = "จำนวน";
            dgvStore.Columns[7].HeaderText = "ราคา";
        }

        private void SearchProduct()
        {
            //Search
            DataTable searchData = ConnectionDB.executsql("SELECT * FROM Store WHERE ISBN LIKE'%" + tbSearch.Text + "%' OR Name LIKE'%" + tbSearch.Text + "%' OR Author LIKE'%" + tbSearch.Text + "%' OR Publisher LIKE'%" + tbSearch.Text + "%' OR PrintYear LIKE'%" + tbSearch.Text + "%' OR Type LIKE'%" + tbSearch.Text + "%' OR Quantity LIKE'%" + tbSearch.Text + "%' OR Cost LIKE'%" + tbSearch.Text + "%' ");
            if (tbSearch.Text == "")
            {
                LoadData();
            }
            else
            {
                dgvStore.DataSource = searchData;
            }
        }

        private void pictMagnifier_Click(object sender, EventArgs e)
        {
            SearchProduct();
        }

        private void pressKeySearch(object sender, KeyPressEventArgs e)
        {
            SearchProduct();
        }

        private void pressKeyUDSearch(object sender, KeyEventArgs e)
        {
            SearchProduct();
        }

        private void pictAdd_Click(object sender, EventArgs e)
        {
            AddProduct addPro = new AddProduct();
            addPro.ShowDialog();
            LoadData();
        }

        private void pictDelete_Click(object sender, EventArgs e)
        {
            // Delete Only 1 row Select
            if (dgvStore.SelectedRows.Count > 1)
            {
                MessageBox.Show("กรุณาเลือกแค่ 1 แถวที่จะลบข้อมูล", "แจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            DialogResult dialogResult = MessageBox.Show("คุณแน่ใจที่จะลบข้อมูลของหนังสือที่เลือกหรือไม่", "แจ้งเตือน", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (dialogResult == DialogResult.Yes)
            {
                ConnectionDB.executsql("DELETE FROM Store WHERE ISBN = '" + dgvStore.SelectedCells[0].Value.ToString() + "'");
                LoadData();
            }
        }

        private void pictEdit_Click(object sender, EventArgs e)
        {
            EditProduct editProduct = new EditProduct();
            editProduct.tbISBN.Text = this.dgvStore.SelectedCells[0].Value.ToString();
            editProduct.tbBookName.Text = this.dgvStore.SelectedCells[1].Value.ToString();
            editProduct.tbAuthor.Text = this.dgvStore.SelectedCells[2].Value.ToString();
            editProduct.tbPublisher.Text = this.dgvStore.SelectedCells[3].Value.ToString();
            editProduct.tbPrintYear.Text = this.dgvStore.SelectedCells[4].Value.ToString();
            editProduct.tbType.Text = this.dgvStore.SelectedCells[5].Value.ToString();
            editProduct.tbQuantity.Text = this.dgvStore.SelectedCells[6].Value.ToString();
            editProduct.tbCost.Text = this.dgvStore.SelectedCells[7].Value.ToString();
            editProduct.ShowDialog();
            LoadData();
        }

        private void pictHome_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        // button color
        private static Color mp = Color.SteelBlue;
        private static Color mpl = Color.LightSkyBlue;
        private void mpAdd(object sender, EventArgs e)
        {
            pictAdd.BackColor = mp;
        }

        private void mplAdd(object sender, EventArgs e)
        {
            pictAdd.BackColor = mpl;
        }

        private void mpDelete(object sender, EventArgs e)
        {
            pictDelete.BackColor = mp;
        }

        private void mplDelete(object sender, EventArgs e)
        {
            pictDelete.BackColor = mpl;
        }

        private void mpEdit(object sender, EventArgs e)
        {
            pictEdit.BackColor = mp;
        }

        private void mplEdit(object sender, EventArgs e)
        {
            pictEdit.BackColor = mpl;
        }

        private void mpHome(object sender, EventArgs e)
        {
            pictHome.BackColor = mp;
        }

        private void mplHome(object sender, EventArgs e)
        {
            pictHome.BackColor = mpl;
        }

        private void mpSearch(object sender, EventArgs e)
        {
            pictMagnifier.BackColor = mp;
        }

        private void mplSearch(object sender, EventArgs e)
        {
            pictMagnifier.BackColor = mpl;
        }

        
    }
}
